
import sys
import os
import pathlib

#
rootDir = os.fspath(pathlib.Path(__file__).parent.resolve())
for path in pathlib.Path(rootDir).iterdir():
    if path.is_dir():
        sys.path.append(str(path))
#

import node_daemon
node_daemon.run()
