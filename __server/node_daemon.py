import socket
import time
from multiprocessing import Process, Manager, Value
import glossary
import overlap_network
import op_file
import log
import multicast_outbound
import multicast_inbound
import subproceso1
import subproceso2
import subproceso3


def run():
    log.info("Arrancando Nodos....")
    data_node = Manager().dict()

    run_subproceso2 = Process(
        target=subproceso2.run, args=(data_node,))
    
    time.sleep(1)

    run_multicast_outbound = Process(
        target=multicast_outbound.run, args=(data_node,))
    
    run_multicast_inbound = Process(
        target=multicast_inbound.run, args=(data_node,))
    
    run_subproceso1 = Process(
         target=subproceso1.run, args=(data_node,))
    
    run_subproceso3 = Process(
        target=subproceso3.run, args=(data_node,))
    

    run_subproceso2.start()
    time.sleep(1)
    
    run_multicast_outbound.start()
    run_multicast_inbound.start()
    run_subproceso1.start()    
    run_subproceso3.start()    

    run_subproceso2.join()
    run_multicast_outbound.join()
    run_multicast_inbound.join()
    run_subproceso1.join()    
    run_subproceso3.join()    
