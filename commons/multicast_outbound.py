import socket
import sys
import struct
import time
import json
import log
import glossary
import datetime
import utils
import overlap_network


#
def run(data_node):
    log.debug(" multicast outbound: up")
    multicast_group = (glossary.ADDRESS_MULTICAST, glossary.PORT_MULTICAST)

    #
    # Create the datagram socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Set a timeout so the socket does not block indefinitely when trying
    # to receive data.
    sock.settimeout(0.2)

    # Set the time-to-live for messages to 1 so they do not go past the
    # local network segment. ..cd cl

    ttl = struct.pack('b', glossary.NODE_TTL)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
    
    while True:
        try:
            message = utils.build_message_multicast(data_node)
            # Send data to the multicast group
            log.debug('multicast outbound: sending "%s"' % message)
            sock.sendto(message.encode(), multicast_group)

            # Look for responses from all recipients
            while True:
                log.debug('multicast outbound: waiting to receive')
                try:
                    data, server = sock.recvfrom(glossary.BUFFER_SIZE)
                    data_json = json.loads(data.decode())
                    log.debug(">>>>>>>>>>>>")
                    log.debug(
                        'multicast outbound recibido ' + str(data_json))
                except socket.timeout:
                    break
                else:
                    log.debug('multicast outbound: received "%s" from %s' %
                              (data, server))

        except KeyboardInterrupt:
            break
        finally:
            time.sleep(1)
    sock.close()



