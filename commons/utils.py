import time
import glossary
from datetime import datetime
import json


#
def dtm_to_str(dtm):
    return dtm.strftime(glossary.FORMAT_DATE)


#
def str_to_dtm(str_date):
    return datetime.strptime(str_date, glossary.FORMAT_DATE)


#
def build_message_multicast(data_node):
    message = {
        glossary.TYPE_COMMUNICATION_FIELD: glossary.MULTICAST,
        glossary.SOURCE_FIELD: {
            glossary.DATETIME_FIELD: dtm_to_str(datetime.now()),
            glossary.NUMBER_APPLICATION_FIELD: data_node[glossary.NUMBER_APPLICATION_FIELD],
            glossary.TTL_FIELD: data_node[glossary.TTL_FIELD],
            glossary.TCP_PORT_INBOUND_FIELD: data_node[glossary.TCP_PORT_INBOUND_FIELD],
            "S": data_node["S"]
        }
    }
    return json.dumps(message)


#
def build_message_tcp_conexion(data_node, command, tcp_port_destiny):
    message = {
        glossary.TYPE_COMMUNICATION_FIELD: glossary.TCP,
        glossary.SOURCE_FIELD: {
            glossary.DATETIME_FIELD: dtm_to_str(datetime.now()),
            glossary.NUMBER_APPLICATION_FIELD: data_node[glossary.NUMBER_APPLICATION_FIELD],
            glossary.TTL_FIELD: data_node[glossary.TTL_FIELD],
            glossary.TCP_PORT_INBOUND_FIELD: data_node[glossary.TCP_PORT_INBOUND_FIELD],
            "S": data_node["S"]
        },
        glossary.TARGET_FIELD: {
            glossary.COMMAND_FIELD: command,
            glossary.TCP_PORT_INBOUND_FIELD: tcp_port_destiny
        }
    }
    return message


#
def print_data_node(data_node):
    print("App: ", data_node[glossary.NUMBER_APPLICATION_FIELD], "TCP ",
            data_node[glossary.TCP_PORT_INBOUND_FIELD], "Next ", data_node[glossary.TCP_PORT_INBOUND_NEXT_FIELD], " S: ",data_node["S"], " L:", data_node["L"])

