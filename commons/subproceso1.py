import socket
import time
import json
import glossary
import log
import utils
import overlap_network
import request_response

from signal import signal, SIGPIPE, SIG_DFL, SIGINT, SIGTERM
signal(SIGPIPE, SIG_DFL)


def run(data_node):

    while True:
        message=utils.build_message_tcp_conexion(data_node,glossary.COMMAND_GIVE_INFO,data_node[glossary.TCP_PORT_INBOUND_NEXT_FIELD])
        request_response.run(message)
        time.sleep(1)
        


