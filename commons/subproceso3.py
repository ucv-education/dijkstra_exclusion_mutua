import socket
import time
import json
import glossary
import log
import overlap_network
from datetime import datetime
import random
import utils
from signal import signal, SIGPIPE, SIG_DFL, SIGINT, SIGTERM
signal(SIGPIPE, SIG_DFL)


def run(data_node):
    while True:
        value_l_equal_s = data_node["S"] == data_node["L"]
        l = data_node["L"]

        if data_node[glossary.NUMBER_APPLICATION_FIELD] == 0:
            
            if value_l_equal_s:
                critical_section(data_node)
                data_node["S"] = l + 1

        else:
            
            if not value_l_equal_s:
                critical_section(data_node)
                data_node["S"] = l


#
def critical_section(data_node):
    
    seconds=random.randint(0, 5)
    f = open("critical_resource.txt", "a")
    f.write("\n")    
    f.write("   Comenzo a escribir la app: " + str(data_node[glossary.NUMBER_APPLICATION_FIELD]))
    f.write("                   timestamp: " + utils.dtm_to_str(datetime.now()))
    f.write("\n")
    f.write("                         S/L: " + str(data_node["S"]) + "/" + str(data_node["L"]))
    f.write("\n")
    f.write("                       ports: " + str(data_node[glossary.TCP_PORT_INBOUND_FIELD]) + "/" + str(data_node[glossary.TCP_PORT_INBOUND_NEXT_FIELD]))
    f.write("\n")
    f.write("   tiempo en recurso critico: " + str(seconds) + " segundos")
    f.write("\n")
    f.write("--------------------------------------------\n" + str(seconds) + " segundos")
    f.write("\n")
    f.write("\n")
    for i in range(1,seconds):
        f.write("segundo nro " + str(i))
        f.write("\n")
        time.sleep(1)
    f.write("-------------------------------------------------------------------------------------------------------------------------------------------")        
    f.write("\n")
    f.close()
