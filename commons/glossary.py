from datetime import datetime
import sys
from pathlib import Path
from os.path import join
import os
 
#
NUMBER_APPLICATION = int(sys.argv[1])
WORKDIR_APPLICATION = sys.argv[2]
DELETE_CRITICAL_RESOURCE_FILE=sys.argv[3]


format_overlap_file="""%s/overlap/%s.json"""
overlap_name_file = format_overlap_file % (WORKDIR_APPLICATION,NUMBER_APPLICATION)
overlap_full_path=join(WORKDIR_APPLICATION, overlap_name_file)
if os.path.exists(overlap_full_path):
    os.remove(overlap_full_path)
    
format_log_file="""%s/logs/log-%s.log"""
name_file = format_log_file % (WORKDIR_APPLICATION,NUMBER_APPLICATION)
log_full_path=join(WORKDIR_APPLICATION, name_file)
if os.path.exists(log_full_path):
    os.remove(log_full_path)
    
if int(DELETE_CRITICAL_RESOURCE_FILE)==1:    
    critical_resource_full_path=join(WORKDIR_APPLICATION, "../critical_resource.txt")
    if os.path.exists(critical_resource_full_path):
        os.remove(critical_resource_full_path)

    
    


FORMAT_DATE = '%d/%m/%y %H:%M:%S'

N=2   # n parameter dijkstra
K=3   # k parameter dijkstra

# Overlap Network
PORT_MULTICAST = 10000
ADDRESS_MULTICAST = '224.3.29.71'
OVERLAP_FILE = overlap_name_file
LOG_FILE = log_full_path
BUFFER_SIZE = 4096
NODE_TTL = 3
DATA_NODE={}
RETRY_TIME=3



# FIELDS
NUMBER_APPLICATION_FIELD="number_application"
TTL_FIELD="ttl"
DATETIME_FIELD="datetime"
HOST_NAME_FIELD="hostname"
ADDRESS_FIELD="address"
TCP_PORT_INBOUND_FIELD="tcp_port_inbound"
TCP_PORT_INBOUND_NEXT_FIELD="tcp_port_inbound_next"
SOURCE_FIELD="source"
TARGET_FIELD="target"
COMMAND_FIELD="command"
TYPE_COMMUNICATION_FIELD="type_communication"

# CONSTANTS

TCP="tcp"
MULTICAST="multicast"


# COMMANDS
COMMAND_GIVE_INFO="GIVE_INFO"
COMMAND_RESPONSE="ACK"





