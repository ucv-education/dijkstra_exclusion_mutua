import socket
import sys
import time
import log
import glossary
import json
import utils


#
def run(data_json):
    retry = glossary.RETRY_TIME
    while retry > 0:
        result_json = execute(data_json)
        if result_json["code"] == 500:
            retry = retry-1
            time.sleep(1)
        else:
            return result_json["data"]

    return None


#
def execute(data_json):

    try:

        data_received = None

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

        tcp_port_destiny = data_json[glossary.TARGET_FIELD][glossary.TCP_PORT_INBOUND_FIELD]

        sock.connect(('', tcp_port_destiny))
        sock.setblocking(True)

        log.info("Send: " + json.dumps(data_json))

        sock.send(json.dumps(data_json).encode())

        data_received_bytes = sock.recv(glossary.BUFFER_SIZE)
        data_received = json.loads(data_received_bytes.decode())

        log.info("Received: " + str(data_received))
    except Exception:
        sock.close()
        return {
            "code": 500
        }
    finally:
        sock.close()

    if not data_received == None and len(data_received) > 0:
        return {
            "code": 200,
            "data": data_received
        }

    return {
        "code": 200,
        "data": None
    }
