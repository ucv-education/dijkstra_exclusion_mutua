import json
import socket
import struct
import time
import log
import glossary
import utils
import overlap_network

#
def run(data_node):

    log.debug("multicast inbound up")

    ANY = "0.0.0.0"
    MCAST_ADDR = glossary.ADDRESS_MULTICAST
    MCAST_PORT = glossary.PORT_MULTICAST

    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)

    # Allow multiple sockets to use the same PORT number
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Bind to the port that we know will receive multicast data
    sock.bind((ANY, MCAST_PORT))

    # Tell the kernel that we are a multicast socket
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 255)

    # Tell the kernel that we want to add ourselves to a multicast group
    # The address for the multicast group is the third param
    sock.setsockopt(socket.IPPROTO_IP,    socket.IP_ADD_MEMBERSHIP,
                             socket.inet_aton(MCAST_ADDR) + socket.inet_aton(ANY))

    # Receive/respond loop
    while True:
        
        log.debug('Multicast_inbound: Esperando para recibir un mensaje')
        data, address = sock.recvfrom(glossary.BUFFER_SIZE)
        data_json=json.loads(data)
        
        log.debug('Multicast_inbound recibido: ' + data.decode())

        overlap_network.set_tcp_port_inbound_next(data_json,data_node)

        message = utils.build_message_multicast(data_node)
        
        log.debug('Multicast_inbound: enviando acknowledgement')
        sock.sendto(message.encode(), address)