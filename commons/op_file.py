import json
import os
import time
from os.path import isfile, join
from datetime import datetime
import glossary
from datetime import timedelta
import op_file


#
def exist_file(filename):
    return os.path.exists(filename)



#
def write_file(new_data, filename):
    #
    if type(new_data) == str or type(new_data) == list or type(new_data) == dict:
        with open(filename, 'w', encoding='utf-8') as f:
            json.dump(new_data, f, ensure_ascii=False, indent=4)
    if type(new_data) == bytes:
        with open(filename, 'wb') as f:
            f.write(new_data)


#
def read_file(filename, type_return=dict):
    data_loaded = None
    try:
        with open(filename) as data_file:
            if(type_return == dict):
                data_loaded = json.load(data_file)
            else:
                return data_file
    except Exception:
        data_loaded
    return data_loaded