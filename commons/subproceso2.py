import socket
import time
import json
import glossary
import log
import overlap_network

from signal import signal, SIGPIPE, SIG_DFL, SIGINT, SIGTERM
signal(SIGPIPE, SIG_DFL)


def run(data_node):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    sock.bind(('', 0))
    sock.listen(1)

    # Para indicar a los clientes por cual puerto deben enviar la informacion
    data_node[glossary.HOST_NAME_FIELD] = socket.gethostname()
    data_node[glossary.ADDRESS_FIELD] = socket.gethostbyname(socket.gethostname())
    data_node[glossary.NUMBER_APPLICATION_FIELD] = glossary.NUMBER_APPLICATION
    data_node[glossary.TCP_PORT_INBOUND_FIELD] = sock.getsockname()[1]
    data_node[glossary.TTL_FIELD] = glossary.NODE_TTL
    
    data_node[glossary.TCP_PORT_INBOUND_NEXT_FIELD] = -1
    data_node["S"] = 0
    data_node["L"] = 0    


    log.info("nodo up")
    log.info("   hostname: " + socket.gethostname())
    log.info("   ip      : " + socket.gethostbyname(socket.gethostname()))
    log.info("   port    : " + str(sock.getsockname()[1]))

    sc, addr = sock.accept()
    data=""

    try:
        while True:
                data = sc.recv(glossary.BUFFER_SIZE)
                data_json = json.loads(data)
                overlap_network.update_l(data_json,data_node)
                response = {
                    "command": glossary.COMMAND_RESPONSE
                }
                sc.send(json.dumps(response).encode())

                sc, addr = sock.accept()

    except Exception as e:
        print("Server shutdown " + str(e) + " -- " + data)
        log.error("Server shutdown " + str(e) )
            
