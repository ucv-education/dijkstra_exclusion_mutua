import logging
from datetime import datetime
import socket
import glossary
import utils
import os
from os.path import join

# format_log_file="""%s/logs/log-%s.log"""
# name_file = format_log_file % (glossary.WORKDIR_APPLICATION,glossary.NUMBER_APPLICATION)
# full_path=join(glossary.WORKDIR_APPLICATION, name_file)
# if os.path.exists(full_path):
#     os.remove(full_path)

logging.basicConfig(filename=glossary.LOG_FILE, encoding='utf-8', level=logging.DEBUG)

def debug(msg): 
    logging.debug(str(datetime.now()) + "  " + msg)    
def info(msg):
    logging.info(str(datetime.now()) + "  " + msg)    
def warning(msg):
    logging.warning(str(datetime.now()) + "  " + msg)    
def error(msg):
    logging.error(str(datetime.now()) + "  " + msg)                
    