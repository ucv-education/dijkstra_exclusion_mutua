#!/usr/bin/env python

import os
import errno
import select
import socket
import json
import psutil
import sys
import struct
import time
import encrypt
import random
import string
import log
import glossary
import utils
import op_command
import overlap_network


def run(data_node):

    log.debug("run_tcp_outbound up")

    while True:

        try:

            request = check_commands(data_node)
            
            if request==None:
                continue
            
            tcp_port_to_destinity = -1
            hostname_destinity=''

            if request["initiator"]["type"] == glossary.SERVER:
                # send to client
                hostname_destinity = request["initiator"]["hostname"]
                tcp_port_to_destinity = request["initiator"]["tcp_port_inbound"]
            else:
                # send to server
                server = overlap_network.get_enable_server()
                if server == -1: continue
                hostname_destinity = server["hostname"]
                tcp_port_to_destinity = server["tcp_port_inbound"]

            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
            
            sock.connect((hostname_destinity, tcp_port_to_destinity))
            sock.setblocking(False)

            json_msg = json.dumps(request)
            
            log.debug("Mensaje Enviado (" + str(tcp_port_to_destinity) + "): "+ json_msg)

            data = encrypt.encrypt_message(str(json_msg))
            sock.sendall(data)
            #


        except Exception as e:
            log.error("Error en transmision de datos a servidor " + str(e))

        finally:
            sock.close()
            
    return None


def check_commands(data_node):
    try:
        while True:
            
            request = None
            log.debug("check commands outbound")
            commands = op_command.read_commands_info()

            if commands == None:
                time.sleep(1)
                continue
            
            commands_pending = (command for command in commands if command["status"] == glossary.STATUS_PENDING)
            
            if commands_pending==None:
                time.sleep(1)
                continue
            
            for c in commands_pending:
                if c["command"] == glossary.COMMAND_RESPONSE:
                    request = c
                    op_command.change_status(request["id"], glossary.STATUS_OK)
                    request["status"] = glossary.STATUS_OK
                    return request
                else:
                    request = op_command.send(data_node, c)
                    op_command.change_status(request["id"], glossary.STATUS_OK)
                    request["status"] = glossary.STATUS_OK
                    return request

            if not request == None:
                break

            time.sleep(1)
    except Exception as e:
        log.error("error en check_commands " + str(e))
    return None
        
