import op_file
import glossary
import json
import utils
import time
import os
from os.path import join
from datetime import datetime
import log


def set_tcp_port_inbound_next(data_json, data_node):
    
    if data_json[glossary.SOURCE_FIELD][glossary.NUMBER_APPLICATION_FIELD] == next_number_application(data_node):
        
        if data_node[glossary.TCP_PORT_INBOUND_NEXT_FIELD] == 0:
            data_node["S"] = 0  #s initialization status
            
        data_node[glossary.TCP_PORT_INBOUND_NEXT_FIELD] = data_json[glossary.SOURCE_FIELD][glossary.TCP_PORT_INBOUND_FIELD]


def next_number_application(data_node):
    if data_node[glossary.NUMBER_APPLICATION_FIELD] == glossary.N-1:
        return 0
    return data_node[glossary.NUMBER_APPLICATION_FIELD]+1


def update_l(data_json, data_node):
    data_node["L"]=data_json[glossary.SOURCE_FIELD]["S"]
